#pragma once

#include <vector>
#include <memory>

namespace Core {
	template <class T>
	class ReferenceCounter {
	protected:
		//Number of references
		std::vector<std::tr1::shared_ptr<T>> _references;
	public:
		//Add member 
		std::tr1::shared_ptr<T> addMember(T* object) {
			//Create a shared ptr for the object
			std::tr1::shared_ptr<T> sharedPtr(object);
			_references.push_back(sharedPtr);
			return _references[_references.size()-1];
		}

		//Remove member
		std::tr1::shared_ptr<T> removeMember(T* object) {
			//Search for the shared ptr 
			for(int i = 0; i < _references.size(); i++) {
				if(_references[i].get() == object) {
					std::tr1::shared_ptr<T> ptr = _references[i];
					_references.erase(_references.begin() + i);
					return ptr;
				}
			}
			return NULL;
		}

		//Size of references
		unsigned int size() {
			return _references.size();
		}

		//Access members
		const std::tr1::shared_ptr<T> operator[](unsigned int index) {
			return _references[index];
		}
	};
}