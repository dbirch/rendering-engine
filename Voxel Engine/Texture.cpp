#include "Texture.h"

#include <exception>

namespace Core {
	namespace Render {
		//Default ctor
		Texture::Texture(std::string fileLocation)
			: _fileLocation(fileLocation),
			_textureLocation(0) {
			//Generate a new texture location
			glGenTextures(1, &this->_textureLocation);
		}

		//Dctor
		Texture::~Texture() {
			glDeleteTextures(1, &this->_textureLocation);
		}

		//Load texture into memory
		bool Texture::loadTexture(Logger* logger) {
			throw new std::runtime_error("Not implemented yet.");
		}

		//Get file name
		std::string Texture::getFileLocation() const {
			return this->_fileLocation;
		}

		//Get texture location
		GLuint Texture::getTextureLocation() const {
			return this->_textureLocation;
		}	

		//Bind texture
		void Texture::enable() {
			if(_textureLocation)
				glBindTexture(GL_TEXTURE_2D, _textureLocation);
		}
	}
}