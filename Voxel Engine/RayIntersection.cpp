#include "RayIntersection.h"

namespace Core {
	namespace Maths {
		//Ctor
		RayShapeIntersection::RayShapeIntersection(bool collided, float time, glm::vec3 point)
			:_collision(collided),
			_time(time),
			_where(point) { }

		//Get intersection point
		glm::vec3 RayShapeIntersection::getIntersectionPoint() {
			return _where;
		}

		//Get time of intersection
		float RayShapeIntersection::getInteresctionTime() {
			return _time;
		}

		//Get whether intersection occured
		bool RayShapeIntersection::didIntersect() {
			return _collision;
		}
	}
}