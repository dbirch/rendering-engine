#pragma once
#include <vector>

namespace Core {
	namespace Window {
		//Window frame listener
		class IFrameListener {
		public:
			//Frame event
			virtual void frameEvent(float delta) { }

			//Pushing events to a list of frame listeners
			static void frameEvent(float delta, std::vector<IFrameListener*> child) {
				for(unsigned int i = 0; i < child.size(); i++) {
					IFrameListener* childListener = child[i];
					if(childListener == NULL) 
						continue;

					childListener->frameEvent(delta);
				}
			}
		};
	}
}