#include "AxisAlignedBoundingBox.h"

namespace Core {
	namespace Render {
		//Default ctor
		AxisAlignedBoundingBox::AxisAlignedBoundingBox() {
			_min = _max = glm::vec3(0,0,0);
		}

		//Add vertex
		void AxisAlignedBoundingBox::addVertex(const glm::vec3& vertex) {
			if(vertex.x < _min.x)
				_min.x = vertex.x;
			else if(vertex.x > _max.x) 
				_max.x = vertex.x;

			if(vertex.y < _min.y) 
				_min.y = vertex.y;
			else if(vertex.y > _max.y)
				_max.y = vertex.y;
		}

		//Get bounding box
		void AxisAlignedBoundingBox::getBoundingBox(glm::vec3& min, glm::vec3& max) {
			min = _min;
			max = _max;
		}
	}
}