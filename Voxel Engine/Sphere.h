#pragma once

#include <glm\glm.hpp>

namespace Core {
	namespace Maths {
		class Sphere {
		private:
			//Position and radius
			glm::vec3 _position;
			float _radius;
		public:
			//Set position
			void setPosition(const glm::vec3& position);

			//Set radius
			void setRadius(const float& radius);

			//Get position
			glm::vec3 getPosition() const;

			//Get radius
			float getRadius() const;
		};
	}
}