#include "MatrixStack.h"

namespace Core {
	namespace Render {
		//Retrieve the top of the stack
		glm::mat4& MatrixStack::getTopStack() {
			//Check there are items on the stack
			if(_stack.size() > 0) {
				return _stack[_stack.size()-1];
			}

			//Push an item on the stack and return
			_stack.push_back(glm::mat4(1.0f));
			return MatrixStack::getTopStack();
		}

		//Default ctor
		MatrixStack::MatrixStack() {
			//Push matrix
			_stack.push_back(glm::mat4(1.0f));           
		}

		//Push matrix
		void MatrixStack::pushMatrix() {
			//Push identity matrix on stack
			glm::mat4 m(1.0f);
			m = getTopStack() * m;  //Post multiply by the entire stack
			_stack.push_back(m);
		}

		//Pop matrix
		void MatrixStack::popMatrix() {
			//Pop back of stack
			_stack.pop_back();
		}

		//Translate stack
		void MatrixStack::translate(glm::vec3 vec) {
			//Generate a translation matrix and multiply
			glm::mat4 mat = glm::translate(glm::mat4(1.0f), glm::vec3(vec.x, vec.y, vec.z));
			getTopStack() = mat * getTopStack();
		}

		//Rotate stack
		void MatrixStack::rotate(glm::vec3 axis, float angle) {
			//Generate a rotation matrix and multiply
			glm::mat4 mat = glm::rotate(glm::mat4(1.0f), angle, axis);
			getTopStack() = mat * getTopStack();
		}

		//Scale stack
		void MatrixStack::scale(glm::vec3 scale) {
			//Generate a scale matrix and multiply
			glm::mat4 mat = glm::scale(glm::mat4(1.0f), scale);
			getTopStack() = mat * getTopStack();
		}

		//Get inverse of matrix
		glm::mat4 MatrixStack::getInverse() {
			return glm::inverse(getTopStack());
		}

		//Load stack identity
		void MatrixStack::loadIdentity() {
			//Get size of stack and load previous matrix
			int size = _stack.size();

			//Get index of previous item to load identity
			getTopStack() = (size > 1) ? _stack[size - 2] : _stack[0];
		}

		//Transform a vector by the stack
		void MatrixStack::transform(glm::vec3& vec) {
			//Convert the vector into 4d
			glm::vec4 hvec(vec.x, vec.y, vec.z, 1);

			//Transform vector by stack
			hvec = getTopStack() * hvec;

			//Set the vector to the components of the transformed vector
			vec.x = hvec.x;
			vec.y = hvec.y;
			vec.z = hvec.z;
		}
	}
}