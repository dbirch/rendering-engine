#pragma once

#include <glm\glm.hpp>

namespace Core {
	namespace Maths {
		class RayShapeIntersection {
		protected:
			//Intersection properties
			glm::vec3 _where;
			float _time;
			bool _collision;
		public:
			//Ctor
			RayShapeIntersection(bool collided, float time, glm::vec3 point);

			//Get intersection point
			glm::vec3 getIntersectionPoint();

			//Get time of intersection
			float getInteresctionTime();

			//Get whether intersection occured
			bool didIntersect();
		};
	}
}