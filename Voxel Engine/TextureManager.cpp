#include "TextureManager.h"

namespace Core {
	namespace Render {
		//Default ctor
		TextureManager::TextureManager(Logger* logger)
			: _iter(0),
			_logger(logger) { }

		//Load a single texture from the stack, returns false if no textures were loaded.
		bool TextureManager::loadTexture() {
			//Check iterator is in range
			if(_iter > _references.size()-1) {
				return false;
			} else {
				//Iter is in range
				_references[_iter]->loadTexture(_logger);
				return true;
			}
		}
			

		std::tr1::shared_ptr<Texture> TextureManager::findTexture(const std::string& fileLocation, int &index) {
			//TODO: Improve this search for large texture counts - index or binary search
			for(index = 0; index < _references.size(); index++) {
				if(_references[index]->getFileLocation() == fileLocation) {
					return _references[index];
				}
			}
			index = -1;
			return NULL;
		}

		//Add a new texture to the manager
		std::tr1::shared_ptr<Texture> TextureManager::addTexture(std::string fileLocation) {
			//Transform to lower
			std::transform(fileLocation.begin(), fileLocation.end(), fileLocation.begin(), ::tolower);

			//Check the texture does not already exist
			int index;
			std::tr1::shared_ptr<Texture> t = this->findTexture(fileLocation, index);
			if(t != NULL) {
				return t;
			}

			//Create a new texture and add to storage
			Texture* newtex = new Texture(fileLocation);
			_references.addMember(newtex);
			return this->findTexture(fileLocation, index);
		}

		//Remove a texture from the manager
		void TextureManager::removeTexture(std::string fileLocation) {
			//Transform to lower
			std::transform(fileLocation.begin(), fileLocation.end(), fileLocation.begin(), ::tolower);

			//Check the texture does not already exist
			int index;
			std::tr1::shared_ptr<Texture> t = this->findTexture(fileLocation, index);
			if(t != NULL) {
				//Check references
				if(t.use_count() == 2) {
					_references.removeMember(t.get());
				}
			} else {
				//Remove of texture that doesn't exist
				_logger->print("Attempt to remove texture which does not exist", LogPriority::Error);
			}
		}
	}
}