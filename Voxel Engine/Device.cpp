#include "Device.h"

namespace Core {
	//Default ctor
	Device::Device(Logger* logger, Window::WindowOptions options)
		: _logger(logger) {
		//Create texture manager
		_textureMgr = new Render::TextureManager(_logger);

		//Create window
		_window = new Window::Window(_logger, options);
	}

	//Dctor
	Device::~Device() {
		//Clean up resources
		delete _textureMgr;
		delete _window;
	}

	//Get window
	Window::Window* Device::getWindow() {
		return _window;
	}
}
