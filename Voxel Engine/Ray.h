#pragma once

#include <glm\glm.hpp>
#include "RayIntersection.h"
#include "Sphere.h"
#include "Plane.h"

namespace Core {
	namespace Maths {
		//A ray
		class Ray {
		protected:
			//Ray origin
			glm::vec3 _origin;

			//Ray direction
			glm::vec3 _direction;

			//Intersect sphere
			inline static bool intersectSphere(const glm::vec3& spherePosition, float radius, const glm::vec3& rayOrigin, const glm::vec3& rayDirection, float& time, glm::vec3& position);
		
			//Intersect plane
			inline static bool intersectPlane(const glm::vec3& planeNormal, const float& d, const glm::vec3& rayOrigin, const glm::vec3& rayDirection, float& time, glm::vec3& position);
		public:
			//Ctor
			Ray(const glm::vec3& origin,
				const glm::vec3& direction);
			
			//Check intersection with a sphere
			RayShapeIntersection intersectSphere(const Sphere& sphere);

			//Check intersection with a sphere
			static RayShapeIntersection intersectSphere(const glm::vec3& spherePosition, float radius, const glm::vec3& rayOrigin, const glm::vec3& rayDirection);
			
			//Check intersection with a plane
			RayShapeIntersection intersectPlane(const Plane& plane);

			//Check intersection with a plane
			static RayShapeIntersection intersectPlane(const glm::vec3& planeNormal, const float& planeConstant, const glm::vec3& rayOrigin, const glm::vec3& rayDirection);
		};
	}
}