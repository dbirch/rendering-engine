#pragma once

#include <glm\glm.hpp>

namespace Core {
	namespace Maths {
		//Plane faces
		enum FaceDirection {
			FRONT,
			BACK,
			BOTH
		};

		class Plane {
		private:
			//Plane faces
			FaceDirection _face;

			//Plane position
			float _distanceFromOrigin;

			//Plane orientation
			glm::vec3 _normal;
		public:
			//Ctor
			Plane(const glm::vec3& normal, const float& distanceFromOrigin);
			
			//Set plane direction and distance from origin
			//void set(const float& distanceFromOrigin, const glm::vec3& direction);

			//Get distance form origin
			float getConstant() const;

			//Get direction
			glm::vec3 getNormal() const;
		};
	}
}