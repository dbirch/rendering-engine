#include "SceneNode.h"

namespace Core {
	namespace Render {
		//Default ctor
		SceneNode::SceneNode()
			:_position(0,0,0),
			_scale(0,0,0),
			_axisAngle(1, 0, 0, 0),
			_parent(0) { }

		//Dctor
		SceneNode::~SceneNode() {

		}

		//Rendering ops
		void SceneNode::renderChildren(MatrixStack* stack) {
			//Load identity
			stack->loadIdentity();

			//Update stack
			stack->scale(_scale);
			stack->rotate(glm::vec3(_axisAngle.x, _axisAngle.y, _axisAngle.z), _axisAngle.w);
			stack->translate(_position);

			//Push matrix
			stack->pushMatrix();
				for(int i = 0; i < _children.size(); i++) {
					_children[i]->render(stack);
				}
			stack->popMatrix();
		}

		//Set position
		void SceneNode::setPosition(const glm::vec3& position) {
			_position.x = position.x;
			_position.y = position.y;
			_position.z = position.z;
		}

		//Set rotation
		void SceneNode::setRotation(const glm::quat& axisAngle) {
			_axisAngle = axisAngle;
		}

		//Set scale
		void SceneNode::setScale(const glm::vec3& scale) {
			_scale.x = scale.x;
			_scale.y = scale.y;
			_scale.z = scale.z;
		}

		//Get position
		glm::vec3 SceneNode::getPosition() {
			return _position;
		}

		//Get rotation
		glm::quat SceneNode::getAxisAngle() {
			return _axisAngle;
		}

		//Get scale
		glm::vec3 SceneNode::getScale() {
			return _scale;
		}

		//Add child to graph
		void SceneNode::addChild(SceneNode* node) {
			//First check that the node does not already exist in the children
			for(int i = 0; i < _children.size(); i++) {
				if(_children[i] == node) {
					return;
				}
			}

			//Add child
			_children.push_back(node);

			//Set parent
			node->_parent = this;
		}

		//Remove child from graph and returns the node
		void SceneNode::removeChild(SceneNode* node) {
			//Try find the node and remove it
			for(int i = 0; i < _children.size(); i++) {
				if(_children[i] == node) {
					_children.erase(_children.begin() + i);
					break;
				}
			}
		}

		//Return a list of children
		std::vector<SceneNode*> SceneNode::getChildren() {
			return _children;
		}

		//Get parent
		SceneNode* SceneNode::getParent() {
			return _parent;
		}
	}
}