#pragma once

#include <vector>
#include <glm\glm.hpp>
#include "Logger.h"
#include "MatrixStack.h"

#include "FrameEvent.h"
#include "KeyboardEvent.h"
#include "MouseEvent.h"

namespace Core {
	namespace Render {
		class SceneNode {
		protected:
			//Child nodes
			std::vector<SceneNode*> _children;

			//Position
			glm::vec3 _position;

			//Rotation
			glm::quat _axisAngle;

			//Scale
			glm::vec3 _scale;

			//Rendering ops
			void renderChildren(MatrixStack* stack);

			//Parent ptr
			SceneNode* _parent;
		public:
			//Default ctor
			SceneNode();

			//Dctor
			virtual ~SceneNode();

			//Set node position
			void setPosition(const glm::vec3& position);

			//Set node rotation
			void setRotation(const glm::quat& axisAngle);

			//Set node scale
			void setScale(const glm::vec3& scale);
			
			//Get node position
			glm::vec3 getPosition();

			//Get node rotation
			glm::quat getAxisAngle();

			//Get node scale
			glm::vec3 getScale();

			//Get node transform
			glm::mat4 transform();

			//Add child to graph
			void addChild(SceneNode* node);

			//Remove child from graph
			void removeChild(SceneNode* node);

			//List children
			std::vector<SceneNode*> getChildren();

			//Get parent node
			SceneNode* getParent();

			//Render
			virtual void render(MatrixStack* const stack) = 0;

			//Frame event
			virtual void frameEvent(const Window::FrameEvent* const frameEvent) = 0;

			//Keyboard event
			virtual void keyboardEvent(const Window::KeyboardEvent* const keyboardEvent) = 0;

			//Mouse event
			virtual void mouseEvent(const Window::MouseEvent* const mouseEvent) = 0;
		};
	}
}