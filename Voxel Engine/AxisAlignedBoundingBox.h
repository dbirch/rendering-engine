#pragma once

#include <glm\glm.hpp>

namespace Core {
	namespace Render {
		class AxisAlignedBoundingBox {
		protected:
			glm::vec3 _min;
			glm::vec3 _max;
		public:
			//Default ctor
			AxisAlignedBoundingBox();

			//Add vertex
			void addVertex(const glm::vec3& vertex);
			
			//Get box
			void getBoundingBox(glm::vec3& min, glm::vec3& max);
		};
	}
}