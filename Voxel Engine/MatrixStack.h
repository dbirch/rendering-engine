#pragma once

#include <vector>
#include <glm\glm.hpp>
#include <glm\ext.hpp>

namespace Core {
	namespace Render {
		class MatrixStack {
		protected:
			//The stack of 4x4 matrices
			std::vector<glm::mat4> _stack;
        
			//Retrieve top of the stack
			glm::mat4& getTopStack();
		public:
			//Default ctor
			MatrixStack();

			//Stack operations
			void pushMatrix();
			void popMatrix();

			//Stack transformations
			void translate(glm::vec3);
			void rotate(glm::vec3, float angle);
			void scale(glm::vec3);
			glm::mat4 getInverse();

			//Reload top of the stack either with identity or the matrix before it.
			void loadIdentity();

			//Transform a vector by the matrix to retreive new coordinates.
			void transform(glm::vec3 &vec);
		};
	}
}
