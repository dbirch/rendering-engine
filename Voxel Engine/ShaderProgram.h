#pragma once

#include "Logger.h"
#include "Shader.h"

namespace Core {
	namespace Render {
		class ShaderProgram {
		private:
			//Vertex shader
			Shader* _vertexShader;

			//Fragment shader
			Shader* _fragmentShader;
		public:
			//Default ctor
			ShaderProgram(const std::string& vertexLocation = "", const std::string& fragmentShader = "");

			//Dctor
			~ShaderProgram();

			//Load and compile
			void loadProgram(Logger* logger);

			//Get vertex shader
			Shader* getVertexShader() const;

			//Get fragment shader
			Shader* getFragmentShader() const;

			//Equal operator
			const bool operator==(const ShaderProgram& other);
		};
	}
}