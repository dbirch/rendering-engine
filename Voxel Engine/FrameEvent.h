#pragma once

namespace Core {
	namespace Window {
		//Frame event data
		class FrameEvent {
		public:
			//Time passed since the previous frame
			float deltaTime;
		};
	}
}