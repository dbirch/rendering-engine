// Voxel Engine.cpp : Defines the entry point for the console application.
//

#include "Device.h"

int main(int argc, char* argv[]) {
	Core::Window::WindowOptions opts;
	opts.title = "Test";
	opts.height = 600;
	opts.width = 600;
	opts.showFrameRateInTitle = true;

	Core::Logger logger;
	logger.logMask = 1&2&4;
	Core::Device device(&logger, opts);

	Core::Window::Window* window = device.getWindow();
	
	while(window->run()) {

	}
	return 0;
}

