#pragma once

#include <windows.h>
#include <gl\glew.h>
#include <gl\gl.h>
#include "Logger.h"
#include <string>

namespace Core {
	namespace Render {
		//Shader program
		class Shader {
		private:
			//Shader location
			std::string _fileLocation;

			//Memory location
			GLuint _shaderLocation;

			//Source
			std::string _source;

			//Shader type
			GLuint _type;
		public:
			//Ctor
			Shader(const std::string& source, GLuint type);

			//Dctor
			~Shader();

			//Load shader
			void loadShader(Logger* logger);

			//Get shader location
			GLuint getShaderLocation() const;

			//Get file location
			std::string getFileLocation() const;

			//Bind shader
			void enable();
		};
	}
}