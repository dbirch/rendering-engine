#pragma once
#include <windows.h>
#include <gl\gl.h>
#include <string>

#include "Logger.h"

namespace Core {
	namespace Render {
		class Texture {
		private:
			//Texture graphics location
			GLuint _textureLocation;

			//Texture storage location
			std::string _fileLocation;
		public:
			//Default ctor
			Texture(std::string fileLocation);
			
			//Dctor
			~Texture();

			//Load texture into memory
			bool loadTexture(Logger* logger);

			//Get file name
			std::string getFileLocation() const;

			//Get texture location
			GLuint getTextureLocation() const;

			//Enable
			void enable();

			const bool operator==(const Texture& other);
		};
	}
}