#pragma once

#include <vector>
#include "ShaderProgram.h"
#include "Logger.h"
#include "ReferenceCounter.h"

namespace Core {
	namespace Render {
		class ShaderManager {
		private:
			//Shader program list
			ReferenceCounter<ShaderProgram> _references;

			//Logger
			Logger* _logger;

			//Internal iterator
			int _iter;

			//Find a shader
			std::tr1::shared_ptr<ShaderProgram> findShaderProgram(const std::string& vertexLocation, const std::string& fragmentLocation, int& index);
		public:
			//Ctor
			ShaderManager(Logger* logger);

			//Create new shader
			std::tr1::shared_ptr<ShaderProgram> loadShader(std::string vertexLocation, std::string fragmentLocation);

			//Load shader into memory
			bool loadShader();

			//Remove a shader
			void removeShader(const ShaderProgram* shader);

			//Remove a shader
			void removeShader(const std::string& vertexLocation, const std::string& fragmentLocation);
		};
	}
}