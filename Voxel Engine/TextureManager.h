#pragma once

#include "Texture.h"
#include "ReferenceCounter.h"

#include <vector>
#include <algorithm>

namespace Core {
	namespace Render {
		//A singleton texture manager
		class TextureManager{
		private:
			//Texture list
			ReferenceCounter<Texture> _references;

			//Internal iterator
			int _iter;

			//Default ctor
			TextureManager();

			//Find a texture
			std::tr1::shared_ptr<Texture> findTexture(const std::string& fileLocation, int &index);

			//Logger 
			Logger* _logger;
		public:
			//Ctor
			TextureManager(Logger* logger);

			//Load a single texture from the stack, returns false if no textures were loaded.
			bool loadTexture();
			
			//Add a new texture to the manager
			std::tr1::shared_ptr<Texture> addTexture(std::string fileLocation);

			//Remove a texture from the manager
			void removeTexture(std::string fileLocation);

			//Remove a texture from the manager
			void removeTexture(const Texture* texture);
		};
	}
}