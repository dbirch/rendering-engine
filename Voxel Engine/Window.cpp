#include "Window.h"

namespace Core {
	namespace Window {
		Window::Window(Logger* logger, const WindowOptions& options)
			:_options(options),
			_lastDelta(0),
			_lastTime(0),
			_running(true),
			_logger(logger) {
			
			//Create a new window instance
			if( SDL_Init( SDL_INIT_EVERYTHING ) != 0 ) {
				//Display error
				_logger->print("Error starting SDL", LogPriority::Error);

				//Quit
				_running = false;
				return;
			}

			//Set video mode
			_surface = SDL_SetVideoMode(_options.width, _options.height, 32, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_OPENGL);
			if(!_surface) {
				//Error creating surface for rendering
				_logger->print("Error setting video mode", LogPriority::Error);

				//Quit
				_running = false;
				return;
			}

			//Prepare for 3D rendering
			this->prepare3D();
			_logger->print("Window and rendering context initialized", LogPriority::Log);

			//Enable GLEW
			GLenum error = glewInit();
			if(error != GLEW_OK) {
				//Glew failed to start
				std::string errorStr = reinterpret_cast<const char *>( glewGetErrorString(error));
				_logger->print("GLEW failed to start." + errorStr, LogPriority::Error);
			}
			std::string vers = reinterpret_cast<const char*>(glewGetString(GLEW_VERSION));
			_logger->print("GLEW initialized" + vers, LogPriority::Log);
		}

		//Dctor
		Window::~Window() {
			//Free SDL surface
			SDL_FreeSurface(_surface);

			//Quit SDL
			SDL_Quit();
		}

		//Prepare for 3D rendering
		void Window::prepare3D() {
			//Set up OpenGL
			glEnable(GL_DEPTH_TEST);				//Enable depth testing
			glShadeModel(GL_SMOOTH);				//Smooth shading
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//Nice perspective correction
			glEnable(GL_TEXTURE_2D);				//Enable texturing

			//Lighting
			glEnable(GL_LIGHTING);
			glEnable(GL_COLOR_MATERIAL);

			//Clear depths and set up view-port
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
			glViewport(0, 0, _options.width, _options.height);

			//Enable modelview and load identity matrix
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
		}

		//Main window run method
		bool Window::run() {
			//Event handing
			SDL_Event event;
			while(SDL_PollEvent(&event)) {
				this->eventHandling(&event);
			}

			//Get new time and calculate delta time
			unsigned int thisTime = SDL_GetTicks();
			float delta = 0.0f;

			
			if(_lastTime == 0) { //First frame?
				delta = 0.01f;
			} else {
				delta = (float) (thisTime - _lastTime);	//Calculate new delta
				this->_frameRate.updateDelta(delta);
			}

			//Set last time equal to this time
			_lastTime = thisTime;
			_lastDelta = delta;

			//Propogate event


			//Update window caption
			std::ostringstream buffer;
			if(_options.showFrameRateInTitle) { //Show frame rate in title?
				buffer << _options.title << "[" << ceil(this->_frameRate.getFrameRate()) << "]";
			} else {
				buffer << _options.title;
			}
			SDL_WM_SetCaption(buffer.str().c_str(), buffer.str().c_str());

			//Return
			return _running;
		}

		//SDL Event handling
		void Window::eventHandling(SDL_Event* event) {
			switch(event->type) {
			case SDL_QUIT:
				//Quit application
				_running = false;

				break;
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				//Propogate event
				
				break;
			case SDL_MOUSEMOTION:
			case SDL_MOUSEBUTTONDOWN:
			case SDL_MOUSEBUTTONUP:
				//Propogate event
				
				break;
			}
		}
	}
}