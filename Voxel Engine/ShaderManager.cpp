#include "ShaderManager.h"

#include <algorithm>

namespace Core {
	namespace Render {
		//Ctor
		ShaderManager::ShaderManager(Logger* logger)
			: _logger(logger),
			_iter(0) { }

		//Find a shader program
		std::tr1::shared_ptr<ShaderProgram> ShaderManager::findShaderProgram(const std::string& vertexLocation, const std::string& fragmentLocation, int& index) {
			//Search programs		
			for(int i = 0; i < _references.size(); i++) {
				std::tr1::shared_ptr<ShaderProgram> program = _references[i];

				//Check vertex program
				if((program->getVertexShader() == 0 && vertexLocation == "")
					|| program->getVertexShader()->getFileLocation() == vertexLocation) {
					if((program->getFragmentShader() == 0 && fragmentLocation == "")
						|| program->getFragmentShader()->getFileLocation() == fragmentLocation) {
							index = i;
							return program;
					}
				}
			}
			return NULL;
		}

		//Create new shader
		std::tr1::shared_ptr<ShaderProgram> ShaderManager::loadShader(std::string vertexLocation, std::string fragmentLocation) {
			//Create a new shader program
			std::transform(vertexLocation.begin(), vertexLocation.end(), vertexLocation.begin(), ::tolower);
			std::transform(fragmentLocation.begin(), fragmentLocation.end(), fragmentLocation.begin(), ::tolower);

			//Look for existing programs
			int index;
			std::tr1::shared_ptr<ShaderProgram> program = this->findShaderProgram(vertexLocation, fragmentLocation, index);
			if(program != NULL) {
				return program;
			}
			
			//Create new program
			return _references.addMember(new ShaderProgram(vertexLocation, fragmentLocation));
		}

		//Load shader into memory
		bool ShaderManager::loadShader() {
			//Check iterator is in range
			if(_iter > _references.size()-1) {
				return false;
			} else {
				//Iter is in range
				_references[_iter]->loadProgram(_logger);
				return true;
			}
		}

		//Remove a shader
		void ShaderManager::removeShader(const ShaderProgram* shader) {

		}

		//Remove a shader
		void ShaderManager::removeShader(const std::string& vertexLocation, const std::string& fragmentLocation) {

		}
	}
}