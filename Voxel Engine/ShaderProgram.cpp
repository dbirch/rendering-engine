#include "ShaderProgram.h"

namespace Core {
	namespace Render {
		//Default ctor
		ShaderProgram::ShaderProgram(const std::string& vertexLocation, const std::string& fragmentLocation)
			:_vertexShader(0),
			_fragmentShader(0) {
			//Create vertex shader
			if(vertexLocation != "") {
				_vertexShader = new Shader(vertexLocation, GL_VERTEX_SHADER);
			}

			//Create fragment shader
			if(fragmentLocation != "") {
				_fragmentShader = new Shader(fragmentLocation, GL_FRAGMENT_SHADER);
			}
		}

		//Dctor
		ShaderProgram::~ShaderProgram() {
			if(_vertexShader)
				delete _vertexShader;
			if(_fragmentShader)
				delete _fragmentShader;
		}

		//Load and compile
		void ShaderProgram::loadProgram(Logger* logger) {
			if(_vertexShader)
				_vertexShader->loadShader(logger);
			if(_fragmentShader)
				_fragmentShader->loadShader(logger);
		}

		//Get vertex shader
		Shader* ShaderProgram::getVertexShader() const {
			return _vertexShader;
		}

		//Get fragment shader
		Shader* ShaderProgram::getFragmentShader() const {
			return _fragmentShader;
		}

		//Equal operator
		const bool ShaderProgram::operator==(const ShaderProgram& other) {
			return this->_fragmentShader == other._fragmentShader
					&& this->_vertexShader == other._vertexShader;
		}
	}
}