#pragma once

#include <string>

namespace Core {
	namespace Window {
		class WindowOptions {
		public:
			//Window width
			int width;

			//Window height
			int height;

			//Projection mode
			enum ProjectionType {
				StereoGraphic,
				Perspective
			} projectionMode;

			//Perspective field of view
			float perspectiveFOV;

			//Window title
			std::string title;

			//Show frame rate in title
			bool showFrameRateInTitle;
		};
	}
}