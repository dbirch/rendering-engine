#include <vector>

//Class which can accurately record the frame rate given frame delta times.
class FrameRate {
private:
	std::vector<float> deltaArray;
	bool first;
	int accuracy;
	std::vector<float>::iterator iterator;

	//Calculate average delta time
	float averageDelta() {
		float c = 0.0f;
		for(unsigned int i = 0; i < deltaArray.size(); i++) {
			c+= deltaArray[i];
		}
		return c / accuracy;
	}
public:
	//Ctor
	FrameRate() {
		//Set first update
		first = true;
		accuracy = 200;
		for(int i = 0; i < accuracy; i++) {
			deltaArray.push_back(0.0f);
		}
		iterator = deltaArray.begin();
	}

	//Call this every frame to get an accurate frame rate
	void updateDelta(float delta) {
		//If this is the first float, fill the entire array with the value to give an very weak estimate 
		//of the frame rate
		if(first) {
			for(int i = 0; i < accuracy; i++) {
				deltaArray[i] = delta;
			}
			first = false;
			return;
		}

		//Push a float on the end
		*iterator = delta;
		iterator++;
		if(iterator == deltaArray.end()) {
			iterator = deltaArray.begin();
		}
	}

	//Get the frame rate
	float getFrameRate() {
		//Calculate frame rate
		return 1000.0f / this->averageDelta();
	}
};
