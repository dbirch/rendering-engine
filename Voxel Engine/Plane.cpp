#include "Plane.h"

namespace Core {
	namespace Maths {
		//Ctor
		Plane::Plane(const glm::vec3& normal, const float& distanceFromOrigin)
			:_normal(normal), 
			_distanceFromOrigin(distanceFromOrigin) { }

		//Get distance form origin
		float Plane::getConstant() const {
			return _distanceFromOrigin;
		}

		//Get direction
		glm::vec3 Plane::getNormal() const {
			return _normal;
		}
	}
}