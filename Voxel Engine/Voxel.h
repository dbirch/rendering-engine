#pragma once

#include "SceneNode.h"

namespace Core {
	namespace Render {
		class Voxel : public SceneNode {
		protected:
		public:
			//Default ctor
			Voxel();

			//Set voxel size
			void setSize(float size);

			//Render voxel
			void render(MatrixStack* const stack) = 0;
		};
	}
}