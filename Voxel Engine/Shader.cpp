#include "Shader.h"

namespace Core {
	namespace Render {
		//Default ctor
		Shader::Shader(const std::string& location, GLuint type)
			:_fileLocation(location),
			_shaderLocation(0),
			_type(type) {
			//Generate a shader location
			_shaderLocation = glCreateShader(type);
		}

		//Dctor
		Shader::~Shader() {
			//Delete shader
			glDeleteShader(_shaderLocation);
		}

		//Load shader into memory
		void Shader::loadShader(Logger* logger) {
			
		}

		//Get shader location
		GLuint Shader::getShaderLocation() const {
			return _shaderLocation;
		}

		//Get file location
		std::string Shader::getFileLocation() const {
			return _fileLocation;
		}

		//Bind shader
		void Shader::enable() {
			glUseProgram(_shaderLocation);
		}
	}
}