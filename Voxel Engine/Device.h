#pragma once

#include "Window.h"
#include "Logger.h"
#include "TextureManager.h"


namespace Core {
	class Device {
	private:
		//Texture management
		Render::TextureManager* _textureMgr;

		//Scene management

		//Window management
		Window::Window* _window;

		//Logger
		Logger* _logger;
	public:
		//Default ctor
		Device(Logger* logger, Window::WindowOptions options);

		//Dctor
		~Device();

		//Get window
		Window::Window* getWindow();
	};
}