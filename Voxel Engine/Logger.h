#pragma once
#include <string>
#include <iostream>

namespace Core {
	//Log levels
	enum LogPriority {
		Log = 1,
		Message = 2,
		Error = 4
	};

	class Logger {
	public:
		//Print log
		void print(const std::string& msg, LogPriority priority);

		//Log mask
		int logMask;
	};
}