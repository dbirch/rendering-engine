#include "Logger.h"

namespace Core {
	void Logger::print(const std::string& msg, LogPriority priority) {
		if((this->logMask & priority) == priority) {
			//Print message
			std::cout << msg << "(" << priority << ")" << std::endl;

			//Throw errors for stack trace
			if(priority == LogPriority::Error) {
				throw std::logic_error(msg.c_str());
			}
		}
	}
}