#pragma once

#include "FrameRate.h"
#include "WindowOptions.h"
#include <glm\glm.hpp>
#include "Logger.h"
#include <sstream>

#define NO_SDL_GLEXT
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>


namespace Core {
	namespace Window {
		class Window {
		private:
			//Window options
			WindowOptions _options;

			//Rendering surface
			SDL_Surface* _surface;

			//Instance running flag
			bool _running;				

			//Last clock time
			unsigned int _lastTime;

			//Last delta time
			float _lastDelta;

			//Frame rate calculation
			FrameRate _frameRate;

			//Logger
			Logger* _logger;

			//SDL Event handling
			void eventHandling(SDL_Event* event);
		public:
			//Default ctor
			Window(Logger* logger, const WindowOptions& options);

			//Dctor
			~Window();

			//Remotely kill window
			void destroy();

			//Prepare for 3D rendering
			void prepare3D();

			//Prepare for 2D rendering
			void prepare2D();

			//Main window run method
			bool run();

			//Swap buffers
			void swapBuffers();

			//Get window options
			WindowOptions getOptions();

			//Set window options
			void setOptions(const WindowOptions& options);

			//Get delta time
			float getDeltaTime();
		};
	}
}