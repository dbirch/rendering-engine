#pragma once

#include "SceneNode.h"

namespace Core {
	namespace Render {
		//Scene graph manager
		class SceneManager {
		private:
			//Root scene node
			SceneNode* _rootNode;

		public:
			//Default ctor
			SceneManager();
		};
	}
}