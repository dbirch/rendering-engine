#include "Ray.h"

namespace Core {
	namespace Maths {
		//Intersect ray with sphere 
		inline bool Ray::intersectSphere(const glm::vec3& spherePosition, float radius, const glm::vec3& rayOrigin, const glm::vec3& rayDirection, float& time, glm::vec3& position) {
			//Using the quadratic formula to solve
			//sum { at^2(rd^2) + 2bt(rd*ro - rd*spherePos) + ro^2 - ro*spherePos + spherePos^2)} = r^2
			float a = rayDirection.x*rayDirection.x + rayDirection.y*rayDirection.y + rayDirection.z*rayDirection.z;

			float b = 2*(rayDirection.x*rayOrigin.x - rayDirection.x*spherePosition.x
						+ rayDirection.y*rayOrigin.y - rayDirection.y*spherePosition.y
						+ rayDirection.z*rayOrigin.z - rayDirection.z*spherePosition.z);

			float c =	rayOrigin.x*rayOrigin.x - 2*(rayOrigin.x*spherePosition.x) + spherePosition.x*spherePosition.x +
						rayOrigin.y*rayOrigin.y - 2*(rayOrigin.y*spherePosition.y) + spherePosition.y*spherePosition.y +
						rayOrigin.z*rayOrigin.z - 2*(rayOrigin.z*spherePosition.z)  + spherePosition.z*spherePosition.z
						- radius*radius;

			float discriminant = b*b - 4*a*c;
			if(discriminant > FLT_EPSILON) {
				discriminant = sqrt(discriminant);
				float twoA = 2*a;
				if(abs(twoA) > FLT_EPSILON) {
					float tPos = (-b + discriminant) / twoA;
					float tNeg = (-b - discriminant) / twoA;

					time = (abs(tPos) < abs(tNeg)) ? tPos : tNeg;
					position = rayOrigin + rayDirection*time;
					return true;
				}
			}
			return false;
		}

		//Intersect a ray with a plane
		inline bool Ray::intersectPlane(const glm::vec3& planeNormal, const float& d, const glm::vec3& rayOrigin, const glm::vec3& rayDirection, float& time, glm::vec3& position) {
			//Solution to the equation
			//t = (d - ro)/(n*rd)
			float denom = glm::dot(rayDirection, planeNormal);
			if(abs(denom) > FLT_EPSILON) {
				float numerator = d - glm::dot(planeNormal, rayOrigin);
				if(abs(numerator) > FLT_EPSILON) {
					time = numerator / denom;
					position = rayOrigin + rayDirection*time;
				}
			}
			return false;
		}

		//Check intersection with a sphere
		RayShapeIntersection Ray::intersectSphere(const glm::vec3& spherePosition, float radius, const glm::vec3& rayOrigin, const glm::vec3& rayDirection) {
			glm::vec3 collision;
			float time;
			bool intersected = Ray::intersectSphere(spherePosition, radius, rayOrigin, rayDirection, time, collision);
			return RayShapeIntersection(intersected, time, collision);
		}
			
		//Check intersection with a sphere
		RayShapeIntersection Ray::intersectSphere(const Sphere& sphere) {
			glm::vec3 collision;
			float time;
			bool intersected = Ray::intersectSphere(sphere.getPosition(), sphere.getRadius(), this->_origin, this->_direction, time, collision);
			return RayShapeIntersection(intersected, time, collision);
		}

		//Check intersection with a plane
		RayShapeIntersection Ray::intersectPlane(const Plane& plane) {
			glm::vec3 collision;
			float time;
			bool intersected = Ray::intersectPlane(plane.getNormal(), plane.getConstant(), this->_origin, this->_direction, time, collision);
			return RayShapeIntersection(intersected, time, collision);
		}

		//Check intersection with a plane
		RayShapeIntersection Ray::intersectPlane(const glm::vec3& planeNormal, const float& planeConstant, const glm::vec3& rayOrigin, const glm::vec3& rayDirection) {
			glm::vec3 collision;
			float time;
			bool intersected = Ray::intersectPlane(planeNormal, planeConstant, rayOrigin, rayDirection, time, collision);
			return RayShapeIntersection(intersected, time, collision);
		}
	}
}