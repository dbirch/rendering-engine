#include "Sphere.h"

namespace Core {
	namespace Maths {
			//Set position
			void Sphere::setPosition(const glm::vec3& position) {
				this->_position = position;
			}

			//Set radius
			void Sphere::setRadius(const float& radius) {
				this->_radius = radius;
			}

			//Get position
			glm::vec3 Sphere::getPosition() const {
				return this->_position;
			}

			//Get radius
			float Sphere::getRadius() const {
				return this->_radius;
			}
	}
}